import Definition from './CRUD.json'
import Connex from '@vechain/connex'

const connex = new Connex({
  node: 'https://testnet.veblocks.net/',
  network: 'test'
})

const ContractAddress = Definition.networks['5777'].address
const Contract = { connex }

Definition.abi.forEach(method => {
  if (!method.name || method.type !== 'function') {
    return
  }

  if (method.constant) {
    Contract[method.name] = defineConstant(method)
  } else {
    Contract[method.name] = defineSignedRequest(method)
  }
})

function defineConstant (method) {
  return (...args) => connex.thor.account(ContractAddress).method(method).call(...args)
}

function defineSignedRequest (method) {
  return async (...args) => {
    const transferClause = connex.thor.account(ContractAddress).method(method).asClause(...args)
    const transactionInfo = await connex.vendor.sign('tx', [transferClause])
      .gas(3000000)
      .request()

    return transactionInfo
  }
}

export default Contract
